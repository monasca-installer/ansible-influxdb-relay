# InfluxDB-Relay

The following Ansible role installs InfluxDB-Relay. InfluxDB Relay is a high
availability layer for InfluxDB.

For more information please refer to:
https://github.com/influxdata/influxdb-relay

# Variables

run_mode:
 * Deploy - Full run including: [Install, Configure, Start, Status]
 * Install - Just installation. Configuration, starting is skipped
 * Configure - Just configuration. Installation, starting is skipped
 * Start - Starts the service, includes checking status
 * Stop - Stops the service
 * Status - Allows to verify the status

|                 Name                |                                                      Description                                                       |
|-------------------------------------|------------------------------------------------------------------------------------------------------------------------|
| `influxdb_relay_http_addr`          | TCP address to bind to, for HTTP server.                                                                               |
| `influxdb_relay_http_port`          | Port for the HTTP server to bind to.                                                                                   |
| `influxdb_client_port`              | InfluxDB port used for client connections.                                                                             |
| `influxdb_relay_buffer_size_mb`     | An upper limit on how much point data to keep in memory (in MB).                                                       |
| `influxdb_relay_max_batch_size_kb`  | A maximum size on the aggregated batches that will be submitted (in KB).                                               |
| `influxdb_relay_max_delay_interval` | The max delay between retry attempts per backend. The initial retry delay is 500ms and is doubled after every failure. |
| `influxdb_relay_paths_config_dir`   | Path to the configuration directory.                                                                                   |
| `influxdb_relay_paths_log_dir`      | Path to the logs directory.                                                                                            |
| `influxdb_relay_paths_bin`          | Path to the directory containing Influxdb-Relay executable.                                                            |
| `influxdb_relay_write_hosts`        | Array of InfluxDB instances to use as backends for Relay.                                                              |
| `influxdb_relay_user`               | System user which runs the Relay processes.                                                                            |
| `influxdb_relay_group`              | The group to which the `influxdb_relay_user` belongs to.                                                               |
| `influxdb_relay_service_name`       | The name of the service Influxdb-Relay service.                                                                        |
| `influxdb_relay_service_path`       | Path to the Influxdb-Relay service file.                                                                               |
| `influxdb_relay_package_name`       | The name of the Influxdb-Relay package.                                                                                |
| `influxdb_relay_wait_for_port_time` | Maximum number of seconds to wait for the service to start during installation.                                        |
| `influxdb_relay_install_mode`       | Can be either `custom_build` to install a custom package or `distribution` to install using package manager.           |
| `influxdb_relay_download_url`       | Location of the package to download. Relevant when `influxdb_relay_install_mode` is set to `custom_build`.             |

# License

Apache License, Version 2.0

# Author Information

Zawisza Hodzic
